package com.hotmail.fabiansandberg98.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import com.hotmail.fabiansandberg98.SettingsManager;

public class chatcolor implements Listener {

	@EventHandler
	public void onPlayerChat(AsyncPlayerChatEvent event) {
		Player player = event.getPlayer();
		if (SettingsManager.getInstance().getConfig()
				.getBoolean("use_chatcolor")) {
			if (player.hasPermission("orcrist.chatcolor")) {
				event.setMessage(event.getMessage().replaceAll(
						"(&([a-fkmolnr0-9]))", "\u00A7$2"));
			}
		} else {
			return;
		}
	}
}
