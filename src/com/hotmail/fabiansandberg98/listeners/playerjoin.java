package com.hotmail.fabiansandberg98.listeners;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import com.hotmail.fabiansandberg98.SettingsManager;
import com.hotmail.fabiansandberg98.messages;

public class playerjoin implements Listener {

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event) {
		/*
		 * Join message (first time, motd and display message)
		 */
		Player player = event.getPlayer();

		if (player.hasPlayedBefore()) {

			event.setJoinMessage(messages.EVENTS_PLAYER_JOIN.getMsg().replace(
					"%player%", player.getName()));
		} else {

			event.setJoinMessage(messages.EVENTS_PLAYER_FIRST_JOIN.getMsg()
					.replace("%player%", player.getName()));
		}
		/*
		 * If player has nick, add it
		 */
		if (SettingsManager.getInstance().getData()
				.contains("nicks." + player.getUniqueId())) {
			if (SettingsManager.getInstance().getData()
					.getBoolean("nicks." + player.getUniqueId() + ".colors") == true) {
				player.setDisplayName(SettingsManager.getInstance().getData()
						.getString("nicks." + player.getUniqueId() + ".nick")
						.replaceAll("(&([a-fkmolnr0-9]))", "\u00A7$2")
						+ ChatColor.WHITE);

			} else if (SettingsManager.getInstance().getData()
					.getBoolean("nicks." + player.getUniqueId() + ".colors") == false) {
				player.setDisplayName(SettingsManager.getInstance().getData()
						.getString("nicks." + player.getUniqueId() + ".nick")
						+ ChatColor.WHITE);
			}
		}
	}
}
