package com.hotmail.fabiansandberg98.listeners;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import com.hotmail.fabiansandberg98.SettingsManager;
import com.hotmail.fabiansandberg98.messages;

public class blockprotection implements Listener {
	
	@EventHandler
	public void onPlayerCheckBlock(PlayerInteractEvent event) {
		
		if (SettingsManager.getInstance().getConfig().getBoolean("use_blockprotection")) {
		
		Player player = event.getPlayer();
	    @SuppressWarnings("deprecation")
		Block targetblock = player.getTargetBlock(null, 0);
	    Location location = targetblock.getLocation();
	    
	        if (player.hasPermission("orcrist.admin.check")) {
	        	if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
	        		if (event.getItem().getType().equals(Material.BOOK)) {
	        			
	        	  String blockloc = location.getBlockX() + "=" + location.getBlockZ() + "=" + location.getBlockY() + "=" + location.getWorld().getName();
	        	  
	        	  if (SettingsManager.getInstance().getBlocks().contains(blockloc)) {
	        		  
	        		  if (!SettingsManager.getInstance().getBlocks().getString(blockloc + ".uuid").equalsIgnoreCase("ADMIN")) {
	        		  String getID = SettingsManager.getInstance().getBlocks().getString(blockloc + ".uuid");
	        		  UUID uuID  = UUID.fromString(getID);
	        		  Player fromID = Bukkit.getPlayer(uuID);
	        		  
	        		  player.sendMessage(messages.EVENT_PLAYER_CHECK_BLOCK.getMsg()
	        				  .replace("%placer%", fromID.getName())
	        				  .replace("%x%", ""+location.getBlockX())
	        				  .replace("%y%", ""+location.getBlockY())
	        				  .replace("%z%", ""+location.getBlockZ()));
	        		  } else {
	        			  player.sendMessage(messages.EVENT_PLAYER_CHECK_BLOCK_ADMIN.getMsg());
	        		  }
	        	  } else {
	        		  player.sendMessage(messages.EVENT_PLAYER_CHECK_BLOCK_NO.getMsg());
	        	  }
	          }
	        }
	    }
		}
	}
	
	@EventHandler
	public void onPlayerPlaceBlock(BlockPlaceEvent event) {
		if (SettingsManager.getInstance().getConfig().getBoolean("use_blockprotection")) {
			
			Player player = event.getPlayer();
			Block block = event.getBlock();
			Location location = block.getLocation();
			
			String blockloc = location.getBlockX() + "=" + location.getBlockZ() + "=" + location.getBlockY() + "=" + location.getWorld().getName();
			
			if (block.getType() == Material.SAND || block.getType() == Material.SAPLING || block.getType() == Material.GRAVEL || block.getType() == Material.DIRT) {
				SettingsManager.getInstance().getBlocks().set(blockloc, null);
				SettingsManager.getInstance().saveBlocks();
			} else {
				SettingsManager.getInstance().getBlocks().set(blockloc + ".uuid", player.getUniqueId().toString());
				SettingsManager.getInstance().saveBlocks();
			}
		}
	}
	
	@EventHandler
	public void onPlayerBreakBlock(BlockBreakEvent event) {
		if (SettingsManager.getInstance().getConfig().getBoolean("use_blockprotection")) {
			
			
			Player player = event.getPlayer();
			Block block = event.getBlock();
			Location location = block.getLocation();
			
			String blockloc = location.getBlockX() + "=" + location.getBlockZ() + "=" + location.getBlockY() + "=" + location.getWorld().getName();
			
			if (SettingsManager.getInstance().getBlocks().contains(blockloc)) {
				if (!SettingsManager.getInstance().getBlocks().getString(blockloc + ".uuid").contains("ADMIN")) {
					
					String getID = SettingsManager.getInstance().getBlocks().getString(blockloc + ".uuid");
					UUID uuID  = UUID.fromString(getID);
		    		Player fromID = Bukkit.getPlayer(uuID);
		    		
					if (player.getName() == fromID.getName()) {
					
						SettingsManager.getInstance().getBlocks().set(blockloc, null);
						SettingsManager.getInstance().saveBlocks();
					
				} else {
	        		
					player.sendMessage(messages.EVENT_PLAYER_BREAK_BLOCK.getMsg().replace("%placer%", fromID.getName()));
					event.setCancelled(true);
				}
			} else {
				player.sendMessage(messages.EVENT_PLAYER_CHECK_BLOCK_ADMIN.getMsg());
				event.setCancelled(true);
			}
			}
		}
	}
	
	@EventHandler
	public void onAdminStick(PlayerInteractEvent event) {
		if (SettingsManager.getInstance().getConfig().getBoolean("use_blockprotection")) {
			
			Player player = event.getPlayer();
		    @SuppressWarnings("deprecation")
			Block targetblock = player.getTargetBlock(null, 0);
		    Location location = targetblock.getLocation();
		    
		        if (player.hasPermission("orcrist.admin.stick")) {
		        	  if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
		        		  if (event.getItem().getType().equals(Material.STICK)) {
		        			  
		        	  String blockloc = location.getBlockX() + "=" + location.getBlockZ() + "=" + location.getBlockY() + "=" + location.getWorld().getName();
		        	  
		        	  SettingsManager.getInstance().getBlocks().set(blockloc, null);
		        	  SettingsManager.getInstance().saveBlocks();
		        	  targetblock.setType(Material.AIR);
		          }
		        }
		    }
		}
	}
	
	@EventHandler
	public void onAdminProtectBlock(PlayerInteractEvent event) {
		if (SettingsManager.getInstance().getConfig().getBoolean("use_blockprotection")) {
			
			Player player = event.getPlayer();
		    @SuppressWarnings("deprecation")
			Block targetblock = player.getTargetBlock(null, 0);
		    Location location = targetblock.getLocation();
		    
		        if (player.hasPermission("orcrist.admin.protection")) {
		        	if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
		        		if (event.getItem().getType().equals(Material.PAPER)) {
		        			
		        	  String blockloc = location.getBlockX() + "=" + location.getBlockZ() + "=" + location.getBlockY() + "=" + location.getWorld().getName();
		        	  
		        	  SettingsManager.getInstance().getBlocks().set(blockloc + ".uuid", "ADMIN");
		        	  SettingsManager.getInstance().saveBlocks();
		          }
		        }
		    }
		}
	}
	
	@EventHandler
	public void onAdminProtectRemove(PlayerInteractEvent event) {
		if (SettingsManager.getInstance().getConfig().getBoolean("use_blockprotection")) {
			
			Player player = event.getPlayer();
		    @SuppressWarnings("deprecation")
			Block targetblock = player.getTargetBlock(null, 0);
		    Location location = targetblock.getLocation();
		    String blockloc = location.getBlockX() + "=" + location.getBlockZ() + "=" + location.getBlockY() + "=" + location.getWorld().getName();
		    
		    	if (player.hasPermission("orcrist.admin.remove.protection")) {
		    		 if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
		    			 if (event.getItem().getType().equals(Material.BLAZE_ROD)) {
		        	  SettingsManager.getInstance().getBlocks().set(blockloc, null);
		        	  SettingsManager.getInstance().saveBlocks();
		    		}
		        }
		    }
		}
	}
	
	@EventHandler
	public void onPistonMove(BlockPistonExtendEvent event) {
		if (SettingsManager.getInstance().getConfig().getBoolean("use_blockprotection")) {
			Block block = event.getBlock();
			Location location = block.getLocation();
			String blockloc = location.getBlockX() + "=" + location.getBlockZ() + "=" + location.getBlockY() + "=" + location.getWorld().getName();
			
			if (SettingsManager.getInstance().getBlocks().contains(blockloc)) {
			event.setCancelled(true);
			}
		}
	} 
}