package com.hotmail.fabiansandberg98.commands;

import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.hotmail.fabiansandberg98.SettingsManager;
import com.hotmail.fabiansandberg98.messages;

public class gamemode implements CommandExecutor {

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;

			if (SettingsManager.getInstance().getConfig()
					.getBoolean("commands.gamemode")) {
				if (player.hasPermission("orcrist.gamemode")) {
					if (args.length == 1) {
						if (args[0].equalsIgnoreCase("survival")
								|| args[0].equalsIgnoreCase("s")
								|| args[0].equalsIgnoreCase("0")) {
							if (player.getGameMode() == GameMode.SURVIVAL) {
								player.sendMessage(messages.CMD_GAMEMODE_SAME
										.getMsg());
							} else {
								player.setGameMode(GameMode.SURVIVAL);
								player.sendMessage(messages.CMD_GAMEMODE_MESSAGE
										.getMsg().replace("%gamemode%",
												"survival"));
							}

						} else if (args[0].equalsIgnoreCase("creative")
								|| args[0].equalsIgnoreCase("c")
								|| args[0].equalsIgnoreCase("1")) {
							if (player.getGameMode() == GameMode.CREATIVE) {
								player.sendMessage(messages.CMD_GAMEMODE_SAME
										.getMsg());
							} else {
								player.setGameMode(GameMode.CREATIVE);
								player.sendMessage(messages.CMD_GAMEMODE_MESSAGE
										.getMsg().replace("%gamemode%",
												"creative"));
							}

						} else if (args[0].equalsIgnoreCase("adventure")
								|| args[0].equalsIgnoreCase("a")
								|| args[0].equalsIgnoreCase("2")) {
							if (player.getGameMode() == GameMode.ADVENTURE) {
								player.sendMessage(messages.CMD_GAMEMODE_SAME
										.getMsg());
							} else {
								player.setGameMode(GameMode.ADVENTURE);
								player.sendMessage(messages.CMD_GAMEMODE_MESSAGE
										.getMsg().replace("%gamemode%",
												"adventure"));
							}
						} else {
							player.sendMessage(messages.CMD_GAMEMODE_SAME
									.getMsg());
						}

					} else if (args.length == 2) {
						if (player.hasPermission("orcrist.gamemode.others")) {
							Player target = (Player) player.getServer()
									.getPlayer(args[1]);

							if (target != null) {
								if (target != player) {
									if (args[0].equalsIgnoreCase("survival")
											|| args[0].equalsIgnoreCase("s")
											|| args[0].equalsIgnoreCase("0")) {
										if (target.getGameMode() == GameMode.SURVIVAL) {
											player.sendMessage(messages.CMD_GAMEMODE_SAME
													.getMsg().replace("You",
															target.getName()));
										} else {
											target.setGameMode(GameMode.SURVIVAL);
											target.sendMessage(messages.CMD_GAMEMODE_OTHER
													.getMsg()
													.replace("%gamemode%",
															"survival")
													.replace("%target%",
															target.getName()));
											player.sendMessage(messages.CMD_GAMEMODE_OTHER
													.getMsg().replace(
															"%gamemode%",
															"survival"));
										}

									} else if (args[0]
											.equalsIgnoreCase("creative")
											|| args[0].equalsIgnoreCase("c")
											|| args[0].equalsIgnoreCase("1")) {
										if (target.getGameMode() == GameMode.CREATIVE) {
											player.sendMessage(messages.CMD_GAMEMODE_SAME
													.getMsg().replace("You",
															target.getName()));
										} else {
											target.setGameMode(GameMode.CREATIVE);
											target.sendMessage(messages.CMD_GAMEMODE_OTHER
													.getMsg()
													.replace("%gamemode%",
															"survival")
													.replace("%target%",
															target.getName()));
											player.sendMessage(messages.CMD_GAMEMODE_OTHER
													.getMsg().replace(
															"%gamemode%",
															"creative"));
										}

									} else if (args[0]
											.equalsIgnoreCase("adventure")
											|| args[0].equalsIgnoreCase("a")
											|| args[0].equalsIgnoreCase("2")) {
										if (target.getGameMode() == GameMode.ADVENTURE) {
											player.sendMessage(messages.CMD_GAMEMODE_SAME
													.getMsg().replace("You",
															target.getName()));
										} else {
											target.setGameMode(GameMode.ADVENTURE);
											target.sendMessage(messages.CMD_GAMEMODE_OTHER
													.getMsg()
													.replace("%gamemode%",
															"survival")
													.replace("%target%",
															target.getName()));
											player.sendMessage(messages.CMD_GAMEMODE_OTHER
													.getMsg().replace(
															"%gamemode%",
															"adventure"));
										}
									} else {
										player.sendMessage(messages.CMD_GAMEMODE_WRONG
												.getMsg());
									}

								} else {
									if (args[0].equalsIgnoreCase("survival")
											|| args[0].equalsIgnoreCase("s")
											|| args[0].equalsIgnoreCase("0")) {
										if (player.getGameMode() == GameMode.SURVIVAL) {
											player.sendMessage(messages.CMD_GAMEMODE_SAME
													.getMsg());
										} else {
											player.setGameMode(GameMode.SURVIVAL);
											player.sendMessage(messages.CMD_GAMEMODE_MESSAGE
													.getMsg().replace(
															"%gamemode%",
															"survival"));
										}

									} else if (args[0]
											.equalsIgnoreCase("creative")
											|| args[0].equalsIgnoreCase("c")
											|| args[0].equalsIgnoreCase("1")) {
										if (player.getGameMode() == GameMode.CREATIVE) {
											player.sendMessage(messages.CMD_GAMEMODE_SAME
													.getMsg());
										} else {
											player.setGameMode(GameMode.CREATIVE);
											player.sendMessage(messages.CMD_GAMEMODE_MESSAGE
													.getMsg().replace(
															"%gamemode%",
															"creative"));
										}

									} else if (args[0]
											.equalsIgnoreCase("adventure")
											|| args[0].equalsIgnoreCase("a")
											|| args[0].equalsIgnoreCase("2")) {
										if (player.getGameMode() == GameMode.ADVENTURE) {
											player.sendMessage(messages.CMD_GAMEMODE_SAME
													.getMsg());
										} else {
											player.setGameMode(GameMode.ADVENTURE);
											player.sendMessage(messages.CMD_GAMEMODE_MESSAGE
													.getMsg().replace(
															"%gamemode%",
															"adventure"));
										}
									} else {
										player.sendMessage(messages.CMD_GAMEMODE_SAME
												.getMsg());
									}

								}
							} else {
								player.sendMessage(messages.UNKNOWN_TARGET
										.getMsg());
							}
						} else {
							player.sendMessage(messages.NO_PERMISSION.getMsg());
						}

					} else if (args.length < 1) {
						player.sendMessage(messages.TOO_SHORT_ARGUMENTS
								.getMsg());
					} else if (args.length > 2) {
						player.sendMessage(messages.TOO_MANY_ARGUMENTS.getMsg());
					}
				} else {
					player.sendMessage(messages.NO_PERMISSION.getMsg());
				}
			} else {
				player.sendMessage(messages.DISABLE.getMsg());
			}
		} else {
			sender.sendMessage(messages.CONSOLE.getMsg());
		}
		return false;
	}
}
