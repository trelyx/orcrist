package com.hotmail.fabiansandberg98.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.hotmail.fabiansandberg98.SettingsManager;
import com.hotmail.fabiansandberg98.messages;

public class nick implements CommandExecutor {

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;

			if (SettingsManager.getInstance().getConfig()
					.getBoolean("commands.nick")) {
				if (player.hasPermission("orcrist.nick")) {
					if (args.length == 0) {
						player.sendMessage(messages.TOO_SHORT_ARGUMENTS
								.getMsg());

					} else if (args.length == 1) {
						if (args[0].matches("^.*[^a-zA-Z0-9& ].*$")) {
							player.sendMessage(messages.ALPHANUMERIC_CHARACTERS
									.getMsg());
						} else {
							if (args[0].length() < 4) {
								player.sendMessage(messages.CMD_NICK_SHORT
										.getMsg());
							} else if (args[0].length() > 17) {
								player.sendMessage(messages.CMD_NICK_LONG
										.getMsg());
							} else {

								if (player.hasPermission("orcrist.nick.colors")) {
									player.sendMessage(messages.CMD_NICK_MESSAGE
											.getMsg().replace("%nickname%",
													generate(args[0])));
									player.setDisplayName(generate(args[0])
											+ ChatColor.WHITE);

									/*
									 * Save nick data
									 */
									SettingsManager
											.getInstance()
											.getData()
											.set("nicks."
													+ player.getUniqueId()
													+ ".colors", true);
									SettingsManager
											.getInstance()
											.getData()
											.set("nicks."
													+ player.getUniqueId()
													+ ".nick", args[0]);
									SettingsManager.getInstance().saveData();

								} else {
									player.sendMessage(messages.CMD_NICK_MESSAGE
											.getMsg().replace("%nickname%",
													args[0]));
									player.setDisplayName(args[0]
											+ ChatColor.WHITE);

									/*
									 * Save nick data
									 */
									SettingsManager
											.getInstance()
											.getData()
											.set("nicks."
													+ player.getUniqueId()
													+ ".colors", false);
									SettingsManager
											.getInstance()
											.getData()
											.set("nicks."
													+ player.getUniqueId()
													+ ".nick", args[0]);
									SettingsManager.getInstance().saveData();
								}
							}
						}

					} else if (args.length == 2) {
						if (args[0].matches("^.*[^a-zA-Z0-9& ].*$")) {
							player.sendMessage(messages.ALPHANUMERIC_CHARACTERS
									.getMsg());
						} else {
							if (args[0].length() < 4) {
								player.sendMessage(messages.CMD_NICK_SHORT
										.getMsg());
							} else if (args[0].length() > 17) {
								player.sendMessage(messages.CMD_NICK_LONG
										.getMsg());
							} else {

								if (player.hasPermission("orcrist.nick.others")) {
									Player target = (Player) player.getServer()
											.getPlayer(args[1]);

									if (target != null) {
										if (target == player) {
											if (player
													.hasPermission("orcrist.nick.colors")) {
												player.sendMessage(messages.CMD_NICK_MESSAGE
														.getMsg()
														.replace(
																"%nickname%",
																generate(args[0])));
												player.setDisplayName(generate(args[0]));

												/*
												 * Save nick data
												 */
												SettingsManager
														.getInstance()
														.getData()
														.set("nicks."
																+ player.getUniqueId()
																+ ".colors",
																true);
												SettingsManager
														.getInstance()
														.getData()
														.set("nicks."
																+ player.getUniqueId()
																+ ".nick",
																args[0]);
												SettingsManager.getInstance()
														.saveData();

											} else {
												player.sendMessage(messages.CMD_NICK_MESSAGE
														.getMsg().replace(
																"%nickname%",
																args[0]));
												player.setDisplayName(args[0]);

												/*
												 * Save nick data
												 */
												SettingsManager
														.getInstance()
														.getData()
														.set("nicks."
																+ player.getUniqueId()
																+ ".colors",
																false);
												SettingsManager
														.getInstance()
														.getData()
														.set("nicks."
																+ player.getUniqueId()
																+ ".nick",
																args[0]);
												SettingsManager.getInstance()
														.saveData();
											}
										} else {

											if (player
													.hasPermission("youtube.nick.colors")) {
												player.sendMessage(messages.CMD_NICK_OTHER
														.getMsg()
														.replace(
																"%target%",
																target.getName())
														.replace(
																"%nickname%",
																generate(args[0])));

												target.sendMessage(messages.CMD_NICK_OTHERS
														.getMsg()
														.replace(
																"%player%",
																player.getName())
														.replace(
																"%nickname%",
																generate(args[0])));
												target.setDisplayName(generate(args[0])
														+ ChatColor.WHITE);

												/*
												 * Save nick data
												 */
												SettingsManager
														.getInstance()
														.getData()
														.set("nicks."
																+ target.getUniqueId()
																+ ".colors",
																true);
												SettingsManager
														.getInstance()
														.getData()
														.set("nicks."
																+ target.getUniqueId()
																+ ".nick",
																args[0]);
												SettingsManager.getInstance()
														.saveData();

											} else {
												player.sendMessage(messages.CMD_NICK_OTHER
														.getMsg()
														.replace(
																"%target%",
																target.getName())
														.replace("%nickname%",
																args[0]));

												target.sendMessage(messages.CMD_NICK_OTHERS
														.getMsg()
														.replace(
																"%player%",
																player.getName())
														.replace("%nickname%",
																args[0]));
												target.setDisplayName(args[0]
														+ ChatColor.WHITE);

												/*
												 * Save nick data
												 */
												SettingsManager
														.getInstance()
														.getData()
														.set("nicks."
																+ target.getUniqueId()
																+ ".colors",
																false);
												SettingsManager
														.getInstance()
														.getData()
														.set("nicks."
																+ target.getUniqueId()
																+ ".nick",
																args[0]);
												SettingsManager.getInstance()
														.saveData();
											}
										}
									} else {
										player.sendMessage(messages.UNKNOWN_TARGET
												.getMsg());
									}
								} else {
									player.sendMessage(messages.NO_PERMISSION
											.getMsg());
								}
							}
						}
					} else if (args.length > 2) {
						player.sendMessage(messages.TOO_MANY_ARGUMENTS.getMsg());
					}
				} else {
					player.sendMessage(messages.NO_PERMISSION.getMsg());
				}
			} else {
				player.sendMessage(messages.DISABLE.getMsg());
			}
		} else {
			sender.sendMessage(messages.CONSOLE.getMsg());
		}
		return false;
	}

	public String generate(String color) {
		return color.replaceAll("(&([a-fkmolnr0-9]))", "\u00A7$2");
	}
}
