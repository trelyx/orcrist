package com.hotmail.fabiansandberg98.commands;

import java.util.UUID;

import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.hotmail.fabiansandberg98.SettingsManager;
import com.hotmail.fabiansandberg98.messages;

public class sethome implements CommandExecutor {
	

	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			UUID uuid = player.getUniqueId();
			World world = player.getLocation().getWorld();
			int x = player.getLocation().getBlockX();
			int y = player.getLocation().getBlockY();
			int z = player.getLocation().getBlockZ();
			float ya = player.getLocation().getYaw();
			float pi = player.getLocation().getPitch();

			int maxhomes = SettingsManager.getInstance().getConfig()
					.getInt("homes.max_homes");

			if (SettingsManager.getInstance().getConfig()
					.getBoolean("commands.sethome")) {
				if (player.hasPermission("orcrist.sethome")) {
					if (args.length == 0) {
						if (!(SettingsManager
								.getInstance()
								.getData()
								.getConfigurationSection(
										"homes." + uuid)
								.getKeys(false).size() >= maxhomes)) {
							
						SettingsManager
								.getInstance()
								.getData()
								.set("homes." + uuid + ".home.world",
										world.getName());
						SettingsManager.getInstance().getData()
								.set("homes." + uuid + ".home.x", (int) x);
						SettingsManager.getInstance().getData()
								.set("homes." + uuid + ".home.y", (int) y);
						SettingsManager.getInstance().getData()
								.set("homes." + uuid + ".home.z", (int) z);
						SettingsManager.getInstance().getData()
								.set("homes." + uuid + ".home.ya", ya);
						SettingsManager.getInstance().getData()
								.set("homes." + uuid + ".home.pi", pi);
						SettingsManager.getInstance().saveData();
						player.sendMessage(messages.CMD_SETHOME_MESSAGE
								.getMsg());
						} else {
							player.sendMessage(messages.CMD_SETHOME_MAX.getMsg());
						}
					} else if (args.length == 1) {
						if (isAlphanumeric(args[0])) {
							if (SettingsManager.getInstance().getData()
									.getString("homes") != null) {
								if (!SettingsManager
										.getInstance()
										.getData()
										.getConfigurationSection(
												"homes." + uuid).getKeys(false).contains(args[0].toLowerCase())) {
									if (!(SettingsManager
											.getInstance()
											.getData()
											.getConfigurationSection(
													"homes." + uuid)
											.getKeys(false).size() >= maxhomes)) {
										SettingsManager
												.getInstance()
												.getData()
												.set("homes." + uuid + "."
														+ args[0].toLowerCase()
														+ ".world",
														world.getName());
										SettingsManager
												.getInstance()
												.getData()
												.set("homes." + uuid + "."
														+ args[0].toLowerCase()
														+ ".x", (int) x);
										SettingsManager
												.getInstance()
												.getData()
												.set("homes." + uuid + "."
														+ args[0].toLowerCase()
														+ ".y", (int) y);
										SettingsManager
												.getInstance()
												.getData()
												.set("homes." + uuid + "."
														+ args[0].toLowerCase()
														+ ".z", (int) z);
										SettingsManager
												.getInstance()
												.getData()
												.set("homes." + uuid + "."
														+ args[0].toLowerCase()
														+ ".ya", ya);
										SettingsManager
												.getInstance()
												.getData()
												.set("homes." + uuid + "."
														+ args[0].toLowerCase()
														+ ".pi", pi);
										SettingsManager.getInstance()
												.saveData();
										player.sendMessage(messages.CMD_SETHOME_MESSAGE
												.getMsg());

									} else {
										player.sendMessage(messages.CMD_SETHOME_MAX
												.getMsg());
									}
								} else {
									player.sendMessage(messages.CMD_SETHOME_EXISTS
											.getMsg());
								}
							} else {
								SettingsManager
										.getInstance()
										.getData()
										.set("homes." + uuid + "."
												+ args[0].toLowerCase()
												+ ".world", world.getName());
								SettingsManager
										.getInstance()
										.getData()
										.set("homes." + uuid + "."
												+ args[0].toLowerCase() + ".x",
												(int) x);
								SettingsManager
										.getInstance()
										.getData()
										.set("homes." + uuid + "."
												+ args[0].toLowerCase() + ".y",
												(int) y);
								SettingsManager
										.getInstance()
										.getData()
										.set("homes." + uuid + "."
												+ args[0].toLowerCase() + ".z",
												(int) z);
								SettingsManager
										.getInstance()
										.getData()
										.set("homes." + uuid + "."
												+ args[0].toLowerCase() + ".ya", ya);
								SettingsManager
										.getInstance()
										.getData()
										.set("homes." + uuid + "."
												+ args[0].toLowerCase() + ".pi", pi);
								SettingsManager.getInstance().saveData();
								player.sendMessage(messages.CMD_SETHOME_MESSAGE
										.getMsg());
							}
						} else {
							player.sendMessage(messages.ALPHANUMERIC_CHARACTERS
									.getMsg());
						}
					} else if (args.length > 1) {
						player.sendMessage(messages.TOO_MANY_ARGUMENTS.getMsg());
					}
				} else {
					player.sendMessage(messages.NO_PERMISSION.getMsg());
				}
			} else {
				player.sendMessage(messages.DISABLE.getMsg());
			}
		} else {
			sender.sendMessage(messages.CONSOLE.getMsg());
		}
		return false;
	}

	public boolean isAlphanumeric(String string) {
		for (int i = 0; i < string.length(); i++) {
			char c = string.charAt(i);
			if (c < 0x30 || (c >= 0x3a && c <= 0x40) || (c > 0x5a && c <= 0x60)
					|| c > 0x7a)
				return false;
		}
		return true;
	}
}
