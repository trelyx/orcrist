package com.hotmail.fabiansandberg98.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.hotmail.fabiansandberg98.SettingsManager;
import com.hotmail.fabiansandberg98.messages;

public class setspawn implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;

			if (SettingsManager.getInstance().getConfig()
					.getBoolean("commands.setspawn")) {
				if (player.hasPermission("orcrist.setspawn")) {
					if (!(args.length > 0)) {
						player.sendMessage(messages.CMD_SETSPAWN_MESSAGE
								.getMsg());
						player.getWorld().setSpawnLocation(getLocX(player),
								getLocY(player), getLocZ(player));
					} else {
						player.sendMessage(messages.TOO_MANY_ARGUMENTS.getMsg());
					}
				} else {
					player.sendMessage(messages.NO_PERMISSION.getMsg());
				}
			} else {
				player.sendMessage(messages.DISABLE.getMsg());
			}
		} else {
			sender.sendMessage(messages.CONSOLE.getMsg());
		}
		return false;
	}

	private int getLocX(Player player) {
		return player.getLocation().getBlockX();
	}

	private int getLocY(Player player) {
		return player.getLocation().getBlockY();
	}

	private int getLocZ(Player player) {
		return player.getLocation().getBlockZ();
	}

}
