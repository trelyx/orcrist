package com.hotmail.fabiansandberg98.commands;

import java.util.UUID;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.hotmail.fabiansandberg98.SettingsManager;
import com.hotmail.fabiansandberg98.messages;

public class delhome implements CommandExecutor {

	@SuppressWarnings({ "deprecation" })
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			UUID uuid = player.getUniqueId();

			if (SettingsManager.getInstance().getConfig()
					.getBoolean("commands.delhome")) {
				if (player.hasPermission("orcrist.delhome")) {
							if (SettingsManager.getInstance().getData()
									.getString("homes") != null) {
						if (args.length == 0) {
									if (SettingsManager
									.getInstance()
									.getData()
									.getConfigurationSection(
											"homes." + uuid).getKeys(false).contains("home")) {
										SettingsManager.getInstance().getData().set("homes." + uuid + ".home", null);
										SettingsManager.getInstance().saveData();
							player.sendMessage(messages.CMD_DELHOME_MESSAGE.getMsg());
									} else {
										player.sendMessage(messages.CMD_HOME_MAIN.getMsg());
									}
						} else if (args.length == 1) {
							if (SettingsManager
									.getInstance()
									.getData()
									.getConfigurationSection(
											"homes." + uuid).getKeys(false).contains(args[0].toLowerCase())) {
								
								SettingsManager.getInstance().getData().set("homes." + uuid + "." + args[0].toLowerCase(), null);
								SettingsManager.getInstance().saveData();
								player.sendMessage(messages.CMD_DELHOME_MESSAGE.getMsg());
								
							} else {
								player.sendMessage(messages.CMD_HOME_EXISTS.getMsg());
							}
							
						} else if (args.length == 2) {
							if (player.hasPermission("orcrist.delhome.others")) {
							Player target = (Player) player.getServer().getPlayer(args[1]);
							
							if (target != null) {
								if (target != player) {
								UUID uuid2 = target.getUniqueId();
								if (SettingsManager
									.getInstance()
									.getData()
									.getConfigurationSection(
											"homes." + uuid2).getKeys(false).contains(args[0].toLowerCase())) {
									SettingsManager.getInstance().getData().set("homes." + uuid2 + "." + args[0].toLowerCase(), null);
									SettingsManager.getInstance().saveData();
									player.sendMessage(messages.CMD_DELHOME_OTHER.getMsg().replace("%target%", target.getName()));
									
								} else {
									player.sendMessage(messages.CMD_HOME_EXISTS.getMsg());
								}
								} else {
									if (SettingsManager
											.getInstance()
											.getData()
											.getConfigurationSection(
													"homes." + uuid).getKeys(false).contains(args[0].toLowerCase())) {
											SettingsManager.getInstance().getData().set("homes." + uuid + "." + args[0].toLowerCase(), null);
											SettingsManager.getInstance().saveData();
											player.sendMessage(messages.CMD_DELHOME_MESSAGE.getMsg());
									} else {
										player.sendMessage(messages.CMD_HOME_EXISTS.getMsg());
									}
								}
							} else {
								player.sendMessage(messages.UNKNOWN_TARGET.getMsg());
							}
							} else {
								player.sendMessage(messages.NO_PERMISSION.getMsg());
							}
						} else if (args.length > 2) {
							player.sendMessage(messages.TOO_MANY_ARGUMENTS.getMsg());
						}
					} else {
						player.sendMessage(messages.CMD_HOME_EXISTS.getMsg());
					}
				} else {
					player.sendMessage(messages.NO_PERMISSION.getMsg());
				}
			} else {
				player.sendMessage(messages.DISABLE.getMsg());
			}
		} else {
			sender.sendMessage(messages.CONSOLE.getMsg());
		}
		return false;
	}

}
