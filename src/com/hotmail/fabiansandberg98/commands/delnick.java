package com.hotmail.fabiansandberg98.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.hotmail.fabiansandberg98.SettingsManager;
import com.hotmail.fabiansandberg98.messages;

public class delnick implements CommandExecutor {

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			if (SettingsManager.getInstance().getConfig()
					.getBoolean("commands.delnick")) {
				if (player.hasPermission("orcrist.delnick")) {
					if (args.length == 0) {
						if (SettingsManager.getInstance().getData()
								.contains("nicks." + player.getUniqueId())) {
							player.sendMessage(messages.CMD_DELNICK_MESSAGE
									.getMsg());
							player.setDisplayName(ChatColor.WHITE
									+ player.getName());

							SettingsManager.getInstance().getData()
									.set("nicks." + player.getUniqueId(), null);
							SettingsManager.getInstance().saveData();
						} else {
							player.sendMessage(messages.CMD_DELNICK_NONICK
									.getMsg());
						}

					} else if (args.length == 1) {
						if (player.hasPermission("orcrist.delnick.others")) {

							Player target = (Player) player.getServer()
									.getPlayer(args[0]);
							if (target != null) {
								if (SettingsManager
										.getInstance()
										.getData()
										.contains(
												"nicks." + target.getUniqueId())) {
									if (target == player) {
										player.sendMessage(messages.CMD_DELNICK_MESSAGE
												.getMsg());
										player.setDisplayName(ChatColor.WHITE
												+ player.getName());

										SettingsManager
												.getInstance()
												.getData()
												.set("nicks."
														+ player.getUniqueId(),
														null);
										SettingsManager.getInstance()
												.saveData();

									} else {
										player.sendMessage(messages.CMD_DELNICK_OTHER
												.getMsg().replace("%target%",
														target.getName()));
										target.sendMessage(messages.CMD_DELNICK_OTHERS
												.getMsg().replace("%player%",
														player.getName()));
										target.setDisplayName(ChatColor.WHITE
												+ target.getName());

										SettingsManager
												.getInstance()
												.getData()
												.set("nicks."
														+ target.getUniqueId(),
														null);
										SettingsManager.getInstance()
												.saveData();
									}
								} else {
									player.sendMessage(messages.CMD_DELNICK_NONICK_OTHER
											.getMsg());
								}
							} else {
								player.sendMessage(messages.UNKNOWN_TARGET
										.getMsg());
							}
						} else {
							player.sendMessage(messages.NO_PERMISSION.getMsg());
						}
					} else if (args.length > 1) {
						player.sendMessage(messages.TOO_SHORT_ARGUMENTS
								.getMsg());
					}
				} else {
					player.sendMessage(messages.NO_PERMISSION.getMsg());
				}
			} else {
				player.sendMessage(messages.DISABLE.getMsg());
			}
		} else {
			sender.sendMessage(messages.CONSOLE.getMsg());
		}
		return false;
	}

}
