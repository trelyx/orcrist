package com.hotmail.fabiansandberg98.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.hotmail.fabiansandberg98.SettingsManager;
import com.hotmail.fabiansandberg98.messages;

public class warps implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;

			if (SettingsManager.getInstance().getConfig()
					.getBoolean("commands.warps")) {
			if (player.hasPermission("orcrist.delwarp")) {
				if (args.length == 0) {

					if (SettingsManager.getInstance().getData()
							.getConfigurationSection("warps") != null) {
						String length = "";
						for (String s : SettingsManager.getInstance().getData()
								.getConfigurationSection("warps")
								.getKeys(false)) {
							length = length + "/n" + s;
						}

						if (length.length() > 0) {

							player.sendMessage(messages.CMD_WARPS_MESSAGE
									.getMsg().replace(
											"%warps%",
											SettingsManager
													.getInstance()
													.getData()
													.getConfigurationSection(
															"warps")
													.getKeys(false).toString()
													.replace("[", "")
													.replace("]", "")));

						} else {
							player.sendMessage(messages.CMD_WARPS_NOWARPS
									.getMsg());
						}
					} else {
						player.sendMessage(messages.CMD_WARPS_NOWARPS.getMsg());
					}
				} else {
					player.sendMessage(messages.TOO_MANY_ARGUMENTS.getMsg());
				}
			} else {
				player.sendMessage(messages.NO_PERMISSION.getMsg());
			}
			} else {
				player.sendMessage(messages.DISABLE.getMsg());
			}
		} else {
			sender.sendMessage(messages.CONSOLE.getMsg());
		}
		return false;
	}
}
