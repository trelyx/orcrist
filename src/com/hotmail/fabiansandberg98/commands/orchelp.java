package com.hotmail.fabiansandberg98.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class orchelp implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			
			if(args.length == 0) {
				player.sendMessage(ChatColor.DARK_GREEN + "=================== " + ChatColor.GRAY + "Orcrist help" + ChatColor.DARK_GREEN + " =================== \n"
						+ ChatColor.GRAY + "/heal  - Heal yourself, or others by using /heal <player> \n"
						+ ChatColor.YELLOW + "/feed  - Feed yourself, or others by using /heal <player> \n"
						+ ChatColor.GRAY + "/spawn  - Teleports you to spawn \n"
						+ ChatColor.YELLOW + "/setspawn  - Creates a new spawn at your location \n"
						+ ChatColor.GRAY + "/kill  - Kill yourself, or others by using /kill <player> \n"
						+ ChatColor.YELLOW + "/pm <player> <message>  - Sends a private message to a player \n"
						+ ChatColor.DARK_GREEN + "=================== " + ChatColor.GRAY + "Page 1/4" + ChatColor.DARK_GREEN + " =================== \n");
				
			} else if (args[0].equalsIgnoreCase("1")) {
				player.sendMessage(ChatColor.DARK_GREEN + "=================== " + ChatColor.GRAY + "Orcrist help" + ChatColor.DARK_GREEN + " =================== \n"
						+ ChatColor.GRAY + "/heal  - Heal yourself, or others by using /heal <player> \n"
						+ ChatColor.YELLOW + "/feed  - Feed yourself, or others by using /heal <player> \n"
						+ ChatColor.GRAY + "/spawn  - Teleports you to spawn \n"
						+ ChatColor.YELLOW + "/setspawn  - Creates a new spawn at your location \n"
						+ ChatColor.GRAY + "/kill  - Kill yourself, or others by using /kill <player> \n"
						+ ChatColor.YELLOW + "/pm <player> <message>  - Sends a private message to a player \n"
						+ ChatColor.DARK_GREEN + "=================== " + ChatColor.GRAY + "Page 1/4" + ChatColor.DARK_GREEN + " =================== \n");
				
			} else if (args[0].equalsIgnoreCase("2")) {
				player.sendMessage(ChatColor.DARK_GREEN + "=================== " + ChatColor.GRAY + "Orcrist help" + ChatColor.DARK_GREEN + " =================== \n"
						+ ChatColor.GRAY + "/reply <message>  - Sends a message the last one who sent you a pm \n"
						+ ChatColor.YELLOW + "/tphere <player>  - Teleport player to your location \n"
						+ ChatColor.GRAY + "/tpto <player>  - Teleports you to player's location \n"
						+ ChatColor.YELLOW + "/nick <nick>  - Give yourself a nick, or others by using /nick <nick> <player> \n"
						+ ChatColor.GRAY + "/delnick  - Removes your nick, or others by using /delnick <player> \n"
						+ ChatColor.YELLOW + "/warp <warp>  - Teleports you to a warp \n"
						+ ChatColor.DARK_GREEN + "=================== " + ChatColor.GRAY + "Page 2/4" + ChatColor.DARK_GREEN + " =================== \n");
				
			} else if (args[0].equalsIgnoreCase("3")) {
				player.sendMessage(ChatColor.DARK_GREEN + "=================== " + ChatColor.GRAY + "Orcrist help" + ChatColor.DARK_GREEN + " =================== \n"
						+ ChatColor.GRAY + "/warps  - Gives you a full list of all warps \n"
						+ ChatColor.YELLOW + "/delwarp <warp>  - Removes a warp \n"
						+ ChatColor.GRAY + "/setwarp <warp>  - Creates a new warp at your location \n"
						+ ChatColor.YELLOW + "/kick <player> <reason>  - Kicks a player, and you must give a reason \n"
						+ ChatColor.GRAY + "/gm <mode>  - Gives you a gamemode, or others by using /gm <mode> <player> \n"
						+ ChatColor.YELLOW + "/home  - Teleports you to your default home, or any other home by using /home <home>  - you can also do /home <home> <player>\n"
						+ ChatColor.DARK_GREEN + "=================== " + ChatColor.GRAY + "Page 3/4" + ChatColor.DARK_GREEN + " =================== \n");
				
			} else if (args[0].equalsIgnoreCase("4")) {
				player.sendMessage(ChatColor.DARK_GREEN + "=================== " + ChatColor.GRAY + "Orcrist help" + ChatColor.DARK_GREEN + " =================== \n"
						+ ChatColor.GRAY + "/homes  - Gives you a full list of your homes, you can also do /homes <player> \n"
						+ ChatColor.YELLOW + "/delhome  - Removes your home, or any other home by using /delhome <home>  - you can also do /delhome <home> <player> \n"
						+ ChatColor.GRAY + "/sethome  - Creates a new home at your location, you can name your home by using /sethome <home> \n"
						+ ChatColor.YELLOW + "/m  - m \n"
						+ ChatColor.GRAY + "/m  - m \n"
						+ ChatColor.YELLOW + "/m  - m \n"
						+ ChatColor.DARK_GREEN + "=================== " + ChatColor.GRAY + "Page 4/4" + ChatColor.DARK_GREEN + " =================== \n");
			}
		}
		return false;
	}

}
