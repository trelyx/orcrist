package com.hotmail.fabiansandberg98.commands;

import java.util.UUID;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.hotmail.fabiansandberg98.SettingsManager;
import com.hotmail.fabiansandberg98.messages;

public class homes implements CommandExecutor {

	@SuppressWarnings({ "deprecation" })
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			UUID uuid = player.getUniqueId();

			if (SettingsManager.getInstance().getConfig()
					.getBoolean("commands.homes")) {
				if (player.hasPermission("orcrist.homes")) {
							if (SettingsManager.getInstance().getData()
									.getString("homes") != null) {
						if (args.length == 0) {
									if (SettingsManager
									.getInstance()
									.getData()
									.getConfigurationSection(
											"homes").getKeys(false).toString().contains(uuid.toString())) {
										String length = "";
										for (String s : SettingsManager.getInstance().getData()
												.getConfigurationSection("homes." + uuid)
												.getKeys(false)) {
											length = length + "/n" + s;
										}
										if (length.length() > 0) {
										player.sendMessage(messages.CMD_HOMES_MESSAGE.getMsg().replace("%homes%",
												SettingsManager
												.getInstance()
												.getData()
												.getConfigurationSection(
														"homes." + uuid).getKeys(false).toString()).replace("]", "").replace("[", ""));
										} else {
											player.sendMessage(messages.CMD_HOMES_EXISTS.getMsg());
										}
									} else {
										player.sendMessage(messages.CMD_HOMES_EXISTS.getMsg());
									}
							
						} else if (args.length == 1) {
							if (player.hasPermission("orcrist.homes.others")) {
							Player target = (Player) player.getServer().getPlayer(args[0]);
							if (target != null) {
								if (target != player) {
								UUID uuid2 = target.getUniqueId();
								if (SettingsManager
									.getInstance()
									.getData()
									.getConfigurationSection(
											"homes").getKeys(false)
									.toString()
									.contains(uuid2.toString())) {
									
									String length = "";
									for (String s : SettingsManager.getInstance().getData()
											.getConfigurationSection("homes." + uuid2)
											.getKeys(false)) {
										length = length + "/n" + s;
									}
									
									if (length.length() > 0) {
									
									player.sendMessage(messages.CMD_HOMES_OTHER.getMsg().replace("%thomes%",
									SettingsManager
									.getInstance()
									.getData()
									.getConfigurationSection(
											"homes." + uuid2).getKeys(false).toString()).replace("]", "").replace("[", "")
											.replace("%target%", target.getName()));
									} else {
										player.sendMessage(messages.CMD_HOMES_EXISTS_OTHER.getMsg());
									}
									
								} else {
									player.sendMessage(messages.CMD_HOMES_EXISTS_OTHER.getMsg());
								}
								} else {
									if (SettingsManager
											.getInstance()
											.getData()
											.getConfigurationSection(
													"homes").getKeys(false).toString().contains(uuid.toString())) {
												String length = "";
												for (String s : SettingsManager.getInstance().getData()
														.getConfigurationSection("homes." + uuid)
														.getKeys(false)) {
													length = length + "/n" + s;
												}
												if (length.length() > 0) {
												player.sendMessage(messages.CMD_HOMES_MESSAGE.getMsg().replace("%homes%",
														SettingsManager
														.getInstance()
														.getData()
														.getConfigurationSection(
																"homes." + uuid).getKeys(false).toString()).replace("]", "").replace("[", ""));
												} else {
													player.sendMessage(messages.CMD_HOMES_EXISTS.getMsg());
												}
											} else {
												player.sendMessage(messages.CMD_HOMES_EXISTS.getMsg());
											}
								}
							} else {
								player.sendMessage(messages.UNKNOWN_TARGET.getMsg());
							}
							} else {
								player.sendMessage(messages.NO_PERMISSION.getMsg());
							}
						} else if (args.length > 1) {
							player.sendMessage(messages.TOO_MANY_ARGUMENTS.getMsg());
						}
					} else {
						player.sendMessage(messages.CMD_HOME_EXISTS.getMsg());
					}
				} else {
					player.sendMessage(messages.NO_PERMISSION.getMsg());
				}
			} else {
				player.sendMessage(messages.DISABLE.getMsg());
			}
		} else {
			sender.sendMessage(messages.CONSOLE.getMsg());
		}
		return false;
	}

}
