package com.hotmail.fabiansandberg98.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.hotmail.fabiansandberg98.SettingsManager;
import com.hotmail.fabiansandberg98.messages;

public class kick implements CommandExecutor {

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;

			if (SettingsManager.getInstance().getConfig()
					.getBoolean("commands.kick")) {
				if (player.hasPermission("orcrist.kick")) {
					if (args.length > 1) {
						Player target = (Player) player.getServer().getPlayer(
								args[0]);

						if (target != null) {
							if (!target.hasPermission("orcrist.kick.bypass")) {

								if (target == player) {
									player.kickPlayer(messages.CMD_KICK_HUMOR
											.getMsg());
									player.getServer()
											.broadcastMessage(
													messages.CMD_KICK_BROADCAST
															.getMsg()
															.replace(
																	"%target%",
																	target.getName())
															.replace(
																	"%reason%",
																	this.getReason(args)));

								} else {
									target.kickPlayer(messages.CMD_KICK_MESSAGE
											.getMsg()
											.replace("%player%",
													player.getName())
											.replace("%reason%",
													this.getReason(args)));
									player.getServer()
											.broadcastMessage(
													messages.CMD_KICK_BROADCAST
															.getMsg()
															.replace(
																	"%target%",
																	target.getName())
															.replace(
																	"%reason%",
																	this.getReason(args)));
								}
							} else if (player.hasPermission("orcrist.kick.op")) {
								if (target == player) {
									player.kickPlayer(messages.CMD_KICK_HUMOR
											.getMsg());
									player.getServer()
											.broadcastMessage(
													messages.CMD_KICK_BROADCAST
															.getMsg()
															.replace(
																	"%target%",
																	target.getName())
															.replace(
																	"%reason%",
																	this.getReason(args)));

								} else {
									target.kickPlayer(messages.CMD_KICK_MESSAGE
											.getMsg()
											.replace("%player%",
													player.getName())
											.replace("%reason%",
													this.getReason(args)));
									player.getServer()
											.broadcastMessage(
													messages.CMD_KICK_BROADCAST
															.getMsg()
															.replace(
																	"%target%",
																	target.getName())
															.replace(
																	"%reason%",
																	this.getReason(args)));
								}
							} else {
								player.sendMessage(messages.CMD_KICK_OP
										.getMsg());
							}
						} else {
							player.sendMessage(messages.UNKNOWN_TARGET.getMsg());
						}
					} else if (args.length < 2) {
						player.sendMessage(messages.TOO_SHORT_ARGUMENTS
								.getMsg());
					}
				} else {
					player.sendMessage(messages.NO_PERMISSION.getMsg());
				}
			} else {
				player.sendMessage(messages.DISABLE.getMsg());
			}
		} else {
			sender.sendMessage("Orcrist commands can't be used in console. If you need to kick him in console, then try /bukkit:kick <player> \n"
					+ messages.CONSOLE.getMsg());
		}
		return false;
	}

	private String getReason(String[] args) {
		StringBuilder sb = new StringBuilder();
		for (int i = 1; i < args.length; i++) {
			sb.append(args[i]);
			sb.append(" ");
		}
		return sb.toString().trim();
	}

}
