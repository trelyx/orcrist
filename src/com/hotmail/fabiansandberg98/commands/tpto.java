package com.hotmail.fabiansandberg98.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.hotmail.fabiansandberg98.SettingsManager;
import com.hotmail.fabiansandberg98.messages;

public class tpto implements CommandExecutor {

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;

			if (SettingsManager.getInstance().getConfig()
					.getBoolean("commands.tpto")) {
				if (player.hasPermission("orcrist.tpto")) {
					if (args.length == 0) {
						player.sendMessage(messages.TOO_SHORT_ARGUMENTS
								.getMsg());

					} else if (args.length == 1) {
						Player target = (Player) player.getServer().getPlayer(
								args[0]);
						if (target != null) {
							if (target != player) {
								player.teleport(target.getLocation());

								player.sendMessage(messages.CMD_TPTO_MESSAGE
										.getMsg().replace("%target%",
												target.getName()));
								target.sendMessage(messages.CMD_TPTO_OTHER
										.getMsg().replace("%player%",
												player.getName()));
							} else {
								player.sendMessage(messages.CMD_TPTO_IDIOT
										.getMsg());
							}
						} else {
							player.sendMessage(messages.UNKNOWN_TARGET.getMsg());
						}
					} else if (args.length > 1) {
						player.sendMessage(messages.TOO_MANY_ARGUMENTS.getMsg());
					}
				} else {
					player.sendMessage(messages.NO_PERMISSION.getMsg());
				}
			} else {
				player.sendMessage(messages.DISABLE.getMsg());
			}
		} else {
			sender.sendMessage(messages.CONSOLE.getMsg());
		}
		return false;
	}

}
