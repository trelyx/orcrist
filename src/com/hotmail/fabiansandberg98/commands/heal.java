package com.hotmail.fabiansandberg98.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.hotmail.fabiansandberg98.SettingsManager;
import com.hotmail.fabiansandberg98.messages;

public class heal implements CommandExecutor {

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;

			if (SettingsManager.getInstance().getConfig()
					.getBoolean("commands.heal")) {
				if (player.hasPermission("orcrist.heal")) {
					if (args.length == 0) {
						player.setHealth((double) 20);
						player.sendMessage(messages.CMD_HEAL_MESSAGE.getMsg());

					} else if (args.length == 1) {
						if (player.hasPermission("orcrist.heal.others")) {
							Player target = (Player) player.getServer()
									.getPlayer(args[0]);
							if (target != null) {
								if (target != player) {

									target.setHealth((double) 20);
									target.sendMessage(messages.CMD_HEAL_OTHERS
											.getMsg().replace("%player%",
													player.getName()));
									player.sendMessage(messages.CMD_HEAL_OTHER
											.getMsg().replace("%target%",
													target.getName()));

								} else {
									target.setHealth((double) 20);
									target.sendMessage(messages.CMD_HEAL_MESSAGE
											.getMsg());
								}
							} else {
								player.sendMessage(messages.UNKNOWN_TARGET
										.getMsg());
							}
						} else {
							player.sendMessage(messages.NO_PERMISSION.getMsg());
						}
					} else if (args.length > 1) {
						player.sendMessage(messages.TOO_MANY_ARGUMENTS.getMsg());
					}
				} else {
					player.sendMessage(messages.NO_PERMISSION.getMsg());
				}
			} else {
				player.sendMessage(messages.DISABLE.getMsg());
			}
		} else {
			sender.sendMessage(messages.CONSOLE.getMsg());
		}
		return false;
	}
}
