package com.hotmail.fabiansandberg98.commands;

import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.hotmail.fabiansandberg98.SettingsManager;
import com.hotmail.fabiansandberg98.messages;

public class home implements CommandExecutor {

	@SuppressWarnings({ "deprecation" })
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			UUID uuid = player.getUniqueId();

			if (SettingsManager.getInstance().getConfig()
					.getBoolean("commands.home")) {
				if (player.hasPermission("orcrist.home")) {
							if (SettingsManager.getInstance().getData()
									.getString("homes") != null) {
						if (args.length == 0) {
									if (SettingsManager
									.getInstance()
									.getData()
									.getConfigurationSection(
											"homes." + uuid).getKeys(false)
									.toString()
									.contains("home")) {
							World world = player.getServer().getWorld(this.getWorld(uuid, "home"));
							String home = "home";
							player.teleport(new Location(world, this.getX(uuid, home), this.getY(uuid, home), this.getZ(uuid, home), this.getYa(uuid, home), this.getPi(uuid, home)));
							player.sendMessage(messages.CMD_HOME_MESSAGE.getMsg());
									} else {
										player.sendMessage(messages.CMD_HOME_MAIN.getMsg());
									}
						} else if (args.length == 1) {
							if (SettingsManager
									.getInstance()
									.getData()
									.getConfigurationSection(
											"homes." + uuid).getKeys(false)
									.toString()
									.contains(args[0].toLowerCase())) {
								
								World world = player.getServer().getWorld(this.getWorld(uuid, args[0].toLowerCase()));
								player.teleport(new Location(world, this.getX(uuid, args[0].toLowerCase()), this.getY(uuid, args[0].toLowerCase()), this.getZ(uuid, args[0].toLowerCase()), this.getYa(uuid, args[0].toLowerCase()), this.getPi(uuid, args[0].toLowerCase())));
								player.sendMessage(messages.CMD_HOME_MESSAGE.getMsg());
								
							} else {
								player.sendMessage(messages.CMD_HOME_EXISTS.getMsg());
							}
							
						} else if (args.length == 2) {
							if (player.hasPermission("orcrist.home.others")) {
							Player target = (Player) player.getServer().getPlayer(args[1]);
							UUID uuid2 = target.getUniqueId();
							
							if (target != null) {
								if (SettingsManager
									.getInstance()
									.getData()
									.getConfigurationSection(
											"homes." + uuid2).getKeys(false)
									.toString()
									.contains(args[0].toLowerCase())) {
									World world = player.getServer().getWorld(this.getWorld(uuid2, args[0].toLowerCase()));
									player.teleport(new Location(world, this.getX(uuid2, args[0].toLowerCase()), this.getY(uuid2, args[0].toLowerCase()), this.getZ(uuid2, args[0].toLowerCase()), this.getYa(uuid2, args[0].toLowerCase()), this.getPi(uuid2, args[0].toLowerCase())));
									player.sendMessage(messages.CMD_HOME_OTHER.getMsg().replace("%target%", target.getName()));
									
								} else {
									player.sendMessage(messages.CMD_HOME_EXISTS.getMsg());
								}
							}
							} else {
								player.sendMessage(messages.NO_PERMISSION.getMsg());
							}
						} else if (args.length > 2) {
							player.sendMessage(messages.TOO_MANY_ARGUMENTS.getMsg());
						}
					} else {
						player.sendMessage(messages.CMD_HOME_EXISTS.getMsg());
					}
				} else {
					player.sendMessage(messages.NO_PERMISSION.getMsg());
				}
			} else {
				player.sendMessage(messages.DISABLE.getMsg());
			}
		} else {
			sender.sendMessage(messages.CONSOLE.getMsg());
		}
		return false;
	}
	
	public String getWorld(UUID uuid, String home) {
		return SettingsManager.getInstance().getData().getString("homes." + uuid + "." + home + ".world");
	}
	
	public int getX(UUID uuid, String home) {
		return SettingsManager.getInstance().getData().getInt("homes." + uuid + "." + home + ".x");
	}
	
	public int getY(UUID uuid, String home) {
		return SettingsManager.getInstance().getData().getInt("homes." + uuid + "." + home + ".y");
	}
	
	public int getZ(UUID uuid, String home) {
		return SettingsManager.getInstance().getData().getInt("homes." + uuid + "." + home + ".z");
	}
	
	public float getYa(UUID uuid, String home) {
		return SettingsManager.getInstance().getData().getInt("homes." + uuid + "." + home + ".ya");
	}
	
	public float getPi(UUID uuid, String home) {
		return SettingsManager.getInstance().getData().getInt("homes." + uuid + "." + home + ".pi");
	}

}
