package com.hotmail.fabiansandberg98.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.hotmail.fabiansandberg98.SettingsManager;
import com.hotmail.fabiansandberg98.messages;

public class setwarp implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;

			if (SettingsManager.getInstance().getConfig()
					.getBoolean("commands.setwarp")) {
				if (player.hasPermission("orcrist.setwarp")) {
					if (args.length == 1) {
						if (isAlphanumeric(args[0])) {
							if (!(args[0].length() > 17)) {
								if (!SettingsManager
										.getInstance()
										.getData()
										.contains(
												"warps."
														+ args[0].toLowerCase())) {

									/*
									 * Save warp data
									 */
									SettingsManager
											.getInstance()
											.getData()
											.set("warps."
													+ args[0].toLowerCase()
													+ ".data.world",
													this.getWorld(player));
									SettingsManager
											.getInstance()
											.getData()
											.set("warps."
													+ args[0].toLowerCase()
													+ ".data.x",
													this.getX(player));
									SettingsManager
											.getInstance()
											.getData()
											.set("warps."
													+ args[0].toLowerCase()
													+ ".data.y",
													this.getY(player));
									SettingsManager
											.getInstance()
											.getData()
											.set("warps."
													+ args[0].toLowerCase()
													+ ".data.z",
													this.getZ(player));
									SettingsManager
											.getInstance()
											.getData()
											.set("warps."
													+ args[0].toLowerCase()
													+ ".data.ya",
													this.getYa(player));
									SettingsManager
											.getInstance()
											.getData()
											.set("warps."
													+ args[0].toLowerCase()
													+ ".data.pi",
													this.getPi(player));

									SettingsManager.getInstance().saveData();

									player.sendMessage(messages.CMD_SETWARP_MESSAGE
											.getMsg());
								} else {
									player.sendMessage(messages.CMD_SETWARP_WARP_EXISTS
											.getMsg());
								}
							} else {
								player.sendMessage(messages.CMD_SETWARP_WARP_LONG
										.getMsg());
							}
						} else {
							player.sendMessage(messages.ALPHANUMERIC_CHARACTERS
									.getMsg());
						}
					} else if (args.length < 1) {
						player.sendMessage(messages.TOO_SHORT_ARGUMENTS
								.getMsg());
					} else if (args.length > 1) {
						player.sendMessage(messages.TOO_MANY_ARGUMENTS.getMsg());
					}
				} else {
					player.sendMessage(messages.NO_PERMISSION.getMsg());
				}
			} else {
				player.sendMessage(messages.DISABLE.getMsg());
			}
		} else {
			sender.sendMessage(messages.CONSOLE.getMsg());
		}
		return false;
	}

	public boolean isAlphanumeric(String string) {
		for (int i = 0; i < string.length(); i++) {
			char c = string.charAt(i);
			if (c < 0x30 || (c >= 0x3a && c <= 0x40) || (c > 0x5a && c <= 0x60)
					|| c > 0x7a)
				return false;
		}
		return true;
	}

	private String getWorld(Player player) {
		return player.getLocation().getWorld().getName();
	}

	private int getX(Player player) {
		return (int) player.getLocation().getX();
	}

	private int getY(Player player) {
		return (int) player.getLocation().getY();
	}

	private int getZ(Player player) {
		return (int) player.getLocation().getZ();
	}

	private float getYa(Player player) {
		return (player.getLocation().getYaw());
	}

	private float getPi(Player player) {
		return (player.getLocation().getPitch());
	}
}
