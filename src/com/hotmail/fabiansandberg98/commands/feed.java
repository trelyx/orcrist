package com.hotmail.fabiansandberg98.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.hotmail.fabiansandberg98.SettingsManager;
import com.hotmail.fabiansandberg98.messages;

public class feed implements CommandExecutor {

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;

			if (SettingsManager.getInstance().getConfig()
					.getBoolean("commands.feed")) {
				if (player.hasPermission("orcrist.feed")) {
					if (args.length == 0) {
						player.setFoodLevel(20);
						player.sendMessage(messages.CMD_FEED_MESSAGE.getMsg());

					} else if (args.length == 1) {
						if (player.hasPermission("orcrist.feed.others")) {
							Player target = (Player) player.getServer()
									.getPlayer(args[0]);
							if (target != null) {
								if (target != player) {

									target.setFoodLevel(20);
									target.sendMessage(messages.CMD_FEED_OTHERS
											.getMsg().replace("%player%",
													player.getName()));
									player.sendMessage(messages.CMD_FEED_OTHER
											.getMsg().replace("%target%",
													target.getName()));

								} else {
									target.setFoodLevel(20);
									target.sendMessage(messages.CMD_FEED_MESSAGE
											.getMsg());
								}
							} else {
								player.sendMessage(messages.UNKNOWN_TARGET
										.getMsg());
							}
						} else {
							player.sendMessage(messages.NO_PERMISSION.getMsg());
						}
					} else if (args.length > 1) {
						player.sendMessage(messages.TOO_MANY_ARGUMENTS.getMsg());
					}
				} else {
					player.sendMessage(messages.NO_PERMISSION.getMsg());
				}
			} else {
				player.sendMessage(messages.DISABLE.getMsg());
			}
		} else {
			sender.sendMessage(messages.CONSOLE.getMsg());
		}
		return false;
	}
}
