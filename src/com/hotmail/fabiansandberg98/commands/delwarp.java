package com.hotmail.fabiansandberg98.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.hotmail.fabiansandberg98.SettingsManager;
import com.hotmail.fabiansandberg98.messages;

public class delwarp implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;

			if (SettingsManager.getInstance().getConfig()
					.getBoolean("commands.delwarp")) {
				if (player.hasPermission("orcrist.delwarp")) {
					if (args.length == 1) {
						if (SettingsManager.getInstance().getData()
								.contains("warps." + args[0].toLowerCase())) {

							/*
							 * Remove warp data
							 */
							SettingsManager
									.getInstance()
									.getData()
									.set("warps." + args[0].toLowerCase(), null);

							SettingsManager.getInstance().saveData();

							player.sendMessage(messages.CMD_DELWARP_MESSAGE
									.getMsg());

						} else {
							player.sendMessage(messages.CMD_WARP_UNKNOWN
									.getMsg());
						}
					} else if (args.length < 1) {
						player.sendMessage(messages.TOO_SHORT_ARGUMENTS
								.getMsg());
					} else if (args.length > 1) {
						player.sendMessage(messages.TOO_MANY_ARGUMENTS.getMsg());
					}
				} else {
					player.sendMessage(messages.NO_PERMISSION.getMsg());
				}
			} else {
				player.sendMessage(messages.DISABLE.getMsg());
			}
		} else {
			sender.sendMessage(messages.CONSOLE.getMsg());
		}
		return false;
	}
}