package com.hotmail.fabiansandberg98.commands;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.hotmail.fabiansandberg98.SettingsManager;
import com.hotmail.fabiansandberg98.messages;

public class privatemessage implements CommandExecutor {

	public static HashMap<UUID, UUID> save = new HashMap<UUID, UUID>();

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;

			if (SettingsManager.getInstance().getConfig()
					.getBoolean("commands.privatemessage")) {
				if (player.hasPermission("orcrist.pm")) {
					if (args.length > 1) {
						Player target = (Player) player.getServer().getPlayer(
								args[0]);
						if (target != null) {
							if (target != player) {

								save.put(target.getUniqueId(),
										player.getUniqueId());
								target.sendMessage(messages.CMD_PRIVATEMESSAGE_TARGET
										.getMsg()
										.replace("%player%", player.getName())
										.replace("%message%", this.getMsg(args)));
								player.sendMessage(messages.CMD_PRIVATEMESSAGE_PLAYER
										.getMsg()
										.replace("%target%", target.getName())
										.replace("%message%", this.getMsg(args)));

							} else {
								player.sendMessage(messages.CMD_PRIVATEMESSAGE_IDIOT
										.getMsg());
							}
						} else {
							player.sendMessage(messages.UNKNOWN_TARGET.getMsg());
						}
					} else {
						player.sendMessage(messages.TOO_SHORT_ARGUMENTS
								.getMsg());
					}
				} else {
					player.sendMessage(messages.NO_PERMISSION.getMsg());
				}
			} else {
				player.sendMessage(messages.DISABLE.getMsg());
			}
		} else {
			sender.sendMessage(messages.CONSOLE.getMsg());
		}
		return false;
	}

	private String getMsg(String[] args) {
		StringBuilder sb = new StringBuilder();
		for (int i = 1; i < args.length; i++) {
			sb.append(args[i]);
			sb.append(" ");
		}
		return sb.toString().trim();
	}

}
