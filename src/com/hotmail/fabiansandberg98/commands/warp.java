package com.hotmail.fabiansandberg98.commands;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.hotmail.fabiansandberg98.SettingsManager;
import com.hotmail.fabiansandberg98.messages;

public class warp implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;

			if (SettingsManager.getInstance().getConfig()
					.getBoolean("commands.warp")) {
			if (player.hasPermission("orcrist.warp")) {
				if (args.length == 1) {
					if (SettingsManager.getInstance().getData()
							.contains("warps." + args[0].toLowerCase())) {

						player.teleport(new Location(this.getWorld(player,
								args[0].toLowerCase()), this.getX(player,
								args[0].toLowerCase()), this.getY(player,
								args[0].toLowerCase()), this.getZ(player,
								args[0].toLowerCase()), this.getYa(player,
								args[0].toLowerCase()), this.getPi(player,
								args[0].toLowerCase())));
						player.sendMessage(messages.CMD_WARP_MESSAGE.getMsg());

					} else {
						player.sendMessage(messages.CMD_WARP_UNKNOWN.getMsg());
					}

				} else if (args.length < 1) {
					player.sendMessage(messages.TOO_SHORT_ARGUMENTS.getMsg());
				} else if (args.length > 1) {
					player.sendMessage(messages.TOO_MANY_ARGUMENTS.getMsg());
				}

			} else {
				player.sendMessage(messages.NO_PERMISSION.getMsg());
			}
			} else {
				player.sendMessage(messages.DISABLE.getMsg());
			}
		} else {
			sender.sendMessage(messages.CONSOLE.getMsg());
		}
		return false;
	}

	private World getWorld(Player player, String args) {
		return player.getServer().getWorld(
				SettingsManager.getInstance().getData()
						.getString("warps." + args + ".data.world"));
	}

	private int getX(Player player, String args) {
		return SettingsManager.getInstance().getData()
				.getInt("warps." + args + ".data.x");
	}

	private int getY(Player player, String args) {
		return SettingsManager.getInstance().getData()
				.getInt("warps." + args + ".data.y");
	}

	private int getZ(Player player, String args) {
		return SettingsManager.getInstance().getData()
				.getInt("warps." + args + ".data.z");
	}

	private float getYa(Player player, String args) {
		return SettingsManager.getInstance().getData()
				.getInt("warps." + args + ".data.ya");
	}

	private float getPi(Player player, String args) {
		return SettingsManager.getInstance().getData()
				.getInt("warps." + args + ".data.pi");
	}
}