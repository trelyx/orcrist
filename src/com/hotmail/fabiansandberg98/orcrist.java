package com.hotmail.fabiansandberg98;

import org.bukkit.plugin.java.JavaPlugin;

import com.hotmail.fabiansandberg98.commands.delhome;
import com.hotmail.fabiansandberg98.commands.delnick;
import com.hotmail.fabiansandberg98.commands.delwarp;
import com.hotmail.fabiansandberg98.commands.feed;
import com.hotmail.fabiansandberg98.commands.gamemode;
import com.hotmail.fabiansandberg98.commands.heal;
import com.hotmail.fabiansandberg98.commands.home;
import com.hotmail.fabiansandberg98.commands.homes;
import com.hotmail.fabiansandberg98.commands.kick;
import com.hotmail.fabiansandberg98.commands.kill;
import com.hotmail.fabiansandberg98.commands.nick;
import com.hotmail.fabiansandberg98.commands.orchelp;
import com.hotmail.fabiansandberg98.commands.privatemessage;
import com.hotmail.fabiansandberg98.commands.reply;
import com.hotmail.fabiansandberg98.commands.sethome;
import com.hotmail.fabiansandberg98.commands.setspawn;
import com.hotmail.fabiansandberg98.commands.setwarp;
import com.hotmail.fabiansandberg98.commands.spawn;
import com.hotmail.fabiansandberg98.commands.tphere;
import com.hotmail.fabiansandberg98.commands.tpto;
import com.hotmail.fabiansandberg98.commands.warp;
import com.hotmail.fabiansandberg98.commands.warps;
import com.hotmail.fabiansandberg98.listeners.blockprotection;
import com.hotmail.fabiansandberg98.listeners.blockprotectionextras;
import com.hotmail.fabiansandberg98.listeners.chat;
import com.hotmail.fabiansandberg98.listeners.chatcolor;
import com.hotmail.fabiansandberg98.listeners.playerjoin;

public class orcrist extends JavaPlugin {
	
	@Override
	public void onEnable() {
		getLogger().info("onEnable has started!");

		SettingsManager.getInstance().setup(this);

		this.getCommands();
		this.getListeners();
	}

	@Override
	public void onDisable() {
		getLogger().info("onDisable has started!");

	}

	public void getListeners() {
		// ChatColor
		chatcolor chatcolor = new chatcolor();
		this.getServer().getPluginManager().registerEvents(chatcolor, this);

		// Player join
		playerjoin playerjoin = new playerjoin();
		this.getServer().getPluginManager().registerEvents(playerjoin, this);
		
		// Chat
		chat chat = new chat();
		this.getServer().getPluginManager().registerEvents(chat, this);
		
		/*
		 * 300 000 lines = 10mb file
		 */
		// Blockprotection
		blockprotection blockprotection = new blockprotection();
		this.getServer().getPluginManager().registerEvents(blockprotection, this);
		
		// BlockprotectionExtras
		blockprotectionextras blockprotectionextras = new blockprotectionextras();
		this.getServer().getPluginManager().registerEvents(blockprotectionextras, this);
	}

	public void getCommands() {
		
		// Orcrist help
		orchelp orchelp = new orchelp();
		getCommand("orcrist").setExecutor(orchelp);
		
		// Heal
		heal heal = new heal();
		getCommand("heal").setExecutor(heal);

		// Feed
		feed feed = new feed();
		getCommand("feed").setExecutor(feed);

		// Spawn
		spawn spawn = new spawn();
		getCommand("spawn").setExecutor(spawn);

		// Setspawn
		setspawn setspawn = new setspawn();
		getCommand("setspawn").setExecutor(setspawn);

		// Kill
		kill kill = new kill();
		getCommand("kill").setExecutor(kill);

		// Message
		privatemessage message = new privatemessage();
		getCommand("message").setExecutor(message);

		// Reply
		reply reply = new reply();
		getCommand("reply").setExecutor(reply);

		// Tphere
		tphere tphere = new tphere();
		getCommand("tphere").setExecutor(tphere);

		// Tpto
		tpto tpto = new tpto();
		getCommand("tpto").setExecutor(tpto);

		// Nick
		nick nick = new nick();
		getCommand("nick").setExecutor(nick);

		// Delnick
		delnick delnick = new delnick();
		getCommand("delnick").setExecutor(delnick);

		// Warp
		warp warp = new warp();
		getCommand("warp").setExecutor(warp);

		// Warps
		warps warps = new warps();
		getCommand("warps").setExecutor(warps);

		// Setwarp
		setwarp setwarp = new setwarp();
		getCommand("setwarp").setExecutor(setwarp);

		// Delwarp
		delwarp delwarp = new delwarp();
		getCommand("delwarp").setExecutor(delwarp);
		
		// Kick
		kick kick = new kick();
		getCommand("kick").setExecutor(kick);
		
		// Gamemode
		gamemode gamemode = new gamemode();
		getCommand("gamemode").setExecutor(gamemode);
		
		// Home
		home home = new home();
		getCommand("home").setExecutor(home);
		
		// Homes
		homes homes = new homes();
		getCommand("homes").setExecutor(homes);
		
		// Delhome
		delhome delhome = new delhome();
		getCommand("delhome").setExecutor(delhome);
		// Sethome
		sethome sethome = new sethome();
		getCommand("sethome").setExecutor(sethome);
	}

}
