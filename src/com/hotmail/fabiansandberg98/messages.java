package com.hotmail.fabiansandberg98;

import org.bukkit.ChatColor;

public enum messages {

	// Player has no permission
	NO_PERMISSION(ChatColor.DARK_RED + "You do not have permission to do that!"),
	// Too many arguments
	TOO_MANY_ARGUMENTS(ChatColor.DARK_RED + "Too many arguments!"),
	// Too short arguments
	TOO_SHORT_ARGUMENTS(ChatColor.DARK_RED + "Too short arguments!"),
	// Unknown target
	UNKNOWN_TARGET(ChatColor.DARK_RED + "That player is offline!"),
	// Only alphanumeric characters allowed
	// |�!\"#��$%&/{([)]=}?+\\`�^�~*'-_.:,;<>
	ALPHANUMERIC_CHARACTERS(ChatColor.DARK_RED
			+ "You can only use alphanumeric characters!"),
	// Console instanceof player
	CONSOLE("This command can only be used in-game!"),
	// Wierd stuff, contact me
	WIERD_STUFF(
			"This should not happen, contact me on Skype(neeh98), Bukkit forum(AdobeGFX) or just send me a mail(fabiansandberg98@hotmail.com)"),
			
	// Disabled command
			DISABLE(ChatColor.DARK_RED + "That command has been disabled!"),

	// Command messages
	// Command heal
	CMD_HEAL_MESSAGE(ChatColor.DARK_GREEN + "Healed!"), CMD_HEAL_OTHERS(
			ChatColor.DARK_GREEN + "You got healed by %player%!"), CMD_HEAL_OTHER(
			ChatColor.DARK_GREEN + "You healed %target%!"),

	// Command feed
	CMD_FEED_MESSAGE(ChatColor.DARK_GREEN + "Feeded!"), CMD_FEED_OTHERS(
			ChatColor.DARK_GREEN + "You got feeded by %player%!"), CMD_FEED_OTHER(
			ChatColor.DARK_GREEN + "You feeded %target%!"),

	// Command kill
	CMD_KILL_MESSAGE(ChatColor.DARK_GREEN + "Killed!"), CMD_KILL_OTHERS(
			ChatColor.DARK_GREEN + "You got Killed by %player%!"), CMD_KILL_OTHER(
			ChatColor.DARK_GREEN + "You Killed %target%!"),

	// Command spawn
	CMD_SPAWN_MESSAGE(ChatColor.DARK_GREEN + "Teleported to spawn!"),

	// Command setspawn
	CMD_SETSPAWN_MESSAGE(ChatColor.DARK_GREEN + "You made a new spawn!!"),

	// Command privatemessage and reply
	CMD_PRIVATEMESSAGE_PLAYER(ChatColor.DARK_GREEN + "[you] to [%target%]"
			+ ChatColor.WHITE + " %message%"), CMD_PRIVATEMESSAGE_TARGET(
			ChatColor.DARK_GREEN + "[%player%] to [you]" + ChatColor.WHITE
					+ " %message%"), CMD_PRIVATEMESSAGE_IDIOT(
			ChatColor.DARK_RED + "You can not send a message to your self!"), CMD_REPLY_WHOM(
			ChatColor.DARK_RED + "You have nobody to whom you can reply!"),

	// Command tpto and tphere
	CMD_TPHERE_MESSAGE(ChatColor.DARK_GREEN
			+ "You teleported %target% to yourself!"), CMD_TPHERE_OTHER(
			ChatColor.DARK_GREEN + "You got teleported to %player%!"), CMD_TPTO_MESSAGE(
			ChatColor.DARK_GREEN + "You teleported yourself to %target%!"), CMD_TPTO_OTHER(
			ChatColor.DARK_GREEN + "%player% got teleported to you!"), CMD_TPTO_IDIOT(
			ChatColor.DARK_RED + "You can't teleport to yourself!"),

	// Command nick and delnick
	CMD_NICK_MESSAGE(ChatColor.DARK_GREEN + "Your nick is now "
			+ ChatColor.RESET + "%nickname%" + ChatColor.DARK_GREEN + "!"), CMD_NICK_OTHERS(
			ChatColor.DARK_GREEN + "Your nick was changed to "
					+ ChatColor.RESET + "%nickname% " + ChatColor.DARK_GREEN
					+ "by %player%!"), CMD_NICK_OTHER(ChatColor.DARK_GREEN
			+ "You changed %target%'s nick to " + ChatColor.RESET
			+ "%nickname%" + ChatColor.DARK_GREEN + "!"), CMD_NICK_SHORT(
			ChatColor.DARK_RED + "Nick can not be shorter than 4 characters!"), CMD_NICK_LONG(
			ChatColor.DARK_RED + "Nick can not be longer than 17 characters!"), CMD_DELNICK_MESSAGE(
			ChatColor.DARK_GREEN + "You removed your nick!"), CMD_DELNICK_OTHERS(
			ChatColor.DARK_GREEN + "Your nick was removed by %player%!"), CMD_DELNICK_OTHER(
			ChatColor.DARK_GREEN + "You removed %target%'s nick!"), CMD_DELNICK_NONICK_OTHER(
			ChatColor.DARK_RED + "That player does not have a nick!"), CMD_DELNICK_NONICK(
			ChatColor.DARK_RED + "You do not have a nick!"),

	// Command warp
	CMD_WARP_MESSAGE(ChatColor.DARK_GREEN + "Teleported to warp!"), CMD_WARP_UNKNOWN(
			ChatColor.DARK_RED + "That warp does not exist!"),

	// Command warps
	CMD_WARPS_MESSAGE(ChatColor.DARK_GREEN + "All available warps: "
			+ ChatColor.WHITE + "%warps%"), CMD_WARPS_NOWARPS(
			ChatColor.DARK_RED + "There is no warps!"),

	// Command setwarp
	CMD_SETWARP_MESSAGE(ChatColor.DARK_GREEN + "You made a new warp!"), CMD_SETWARP_WARP_EXISTS(
			ChatColor.DARK_RED + "That warp already exists!"), CMD_SETWARP_WARP_LONG(
			ChatColor.DARK_RED + "Warp can not be longer than 17 characters"),

	// Command delwarp
	CMD_DELWARP_MESSAGE(ChatColor.DARK_GREEN + "You deleted a warp!"),
	
	// Command kick
	CMD_KICK_BROADCAST(ChatColor.DARK_GREEN + "[KICK ALERT] %target% got kicked from the server. \nReason: " + ChatColor.WHITE + "%reason%"),
	CMD_KICK_MESSAGE("You got kicked from the server by %player%. \nReason: %reason%"),
	CMD_KICK_HUMOR("So, you kicked yourself out of the server? Why don't you ban yourself aswell?? :)"),
	CMD_KICK_OP(ChatColor.DARK_RED + "You do not have permission to kick that player!"),
	
	// Command gamemode
	CMD_GAMEMODE_MESSAGE(ChatColor.DARK_GREEN + "You changed your gamemode to %gamemode%!"),
	CMD_GAMEMODE_OTHERS(ChatColor.DARK_GREEN + "Your gamemode what changed to %gamemode%!"),
	CMD_GAMEMODE_OTHER(ChatColor.DARK_GREEN + "Your changed %target%'s gamemode to %gamemode%!"),
	CMD_GAMEMODE_WRONG(ChatColor.DARK_RED + "That gamemode does not exist!"),
	CMD_GAMEMODE_SAME(ChatColor.DARK_RED + "You already got that gamemode!"),
	
	// Command home
	CMD_HOME_MESSAGE(ChatColor.DARK_GREEN + "You got teleported home!"),
	CMD_HOME_EXISTS(ChatColor.DARK_RED + "That home does not exist!"),
	CMD_HOME_OTHER(ChatColor.DARK_GREEN + "You got teleported to %target%'s home!"),
	CMD_HOME_MAIN(ChatColor.DARK_RED + "You don't have a main home!"),
	
	// Command homes
	CMD_HOMES_MESSAGE(ChatColor.DARK_GREEN + "Homes: " + ChatColor.WHITE + "%homes%"),
	CMD_HOMES_OTHER(ChatColor.DARK_GREEN + "%target%'s homes: " + ChatColor.WHITE + "%thomes%"),
	CMD_HOMES_EXISTS(ChatColor.DARK_RED + "You do not have any homes!"),
	CMD_HOMES_EXISTS_OTHER(ChatColor.DARK_RED + "That player does not have any homes!"),
	
	// Command delhome
	CMD_DELHOME_MESSAGE(ChatColor.DARK_GREEN + "You removed your home!"),
	CMD_DELHOME_OTHER(ChatColor.DARK_RED + "You removed %target%'s home!"),
	
	// Command sethome
	CMD_SETHOME_MESSAGE(ChatColor.DARK_GREEN + "You created a new home!"),
	CMD_SETHOME_MAX(ChatColor.DARK_RED + "You can't make more homes!"),
	CMD_SETHOME_EXISTS(ChatColor.DARK_RED + "That home already exists!"),
	
	// Event messages
	// Event onPlayerJoin
	EVENTS_PLAYER_JOIN("%player%" + ChatColor.GREEN + " joined the server."), EVENTS_PLAYER_FIRST_JOIN(
			"%player%" + ChatColor.DARK_PURPLE
					+ " joined the server for the first time!"), EVENTS_PLAYER_JOIN_MOTD(
			ChatColor.GOLD + "Welcome back, " + ChatColor.WHITE + "%player%"
					+ ChatColor.GOLD + ". There is " + ChatColor.WHITE
					+ "%playerlength%" + ChatColor.GOLD + "/" + ChatColor.WHITE
					+ "%maxplayers%" + ChatColor.GOLD
					+ " players online.\nOnline players: " + ChatColor.WHITE
					+ "%playerlist%"),
					
	//Event onPlayerCheckBlock
	EVENT_PLAYER_CHECK_BLOCK(ChatColor.DARK_GREEN + "Block placed by: " + ChatColor.WHITE + "%placer%.\n " + ChatColor.DARK_GREEN + "Location: " + ChatColor.WHITE + "X:%x% Y:%y% Z:%z%."),
	EVENT_PLAYER_CHECK_BLOCK_NO(ChatColor.DARK_RED + "That block is not owned by anyone!"),
	EVENT_PLAYER_CHECK_BLOCK_ADMIN(ChatColor.DARK_RED + "That block is protected by admin!"),
	
	//Event onPlayerBreakBlock
	EVENT_PLAYER_BREAK_BLOCK(ChatColor.DARK_RED + "%placer% owns that block!"),
	
	//Event onPlayerOpenChest
	EVENT_PLAYER_OPEN_CHEST(ChatColor.DARK_RED + "%placer% owns that chest!");

	private String msg;

	messages(String msg) {
		this.setMsg(msg);
	}

	public String getMsg() {
		return this.msg;
	}

	private void setMsg(String msg) {
		this.msg = msg;
	}
}
